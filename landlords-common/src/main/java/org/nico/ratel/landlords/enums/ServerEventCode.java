package org.nico.ratel.landlords.enums;

import org.nico.noson.Noson;
import org.nico.ratel.landlords.channel.ChannelUtils;
import org.nico.ratel.landlords.entity.ClientSide;
import org.nico.ratel.landlords.entity.Poker;
import org.nico.ratel.landlords.entity.PokerSell;
import org.nico.ratel.landlords.entity.Room;
import org.nico.ratel.landlords.helper.MapHelper;
import org.nico.ratel.landlords.helper.PokerHelper;
import org.nico.ratel.landlords.robot.RobotDecisionMakers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public enum ServerEventCode implements Serializable{


	CODE_CLIENT_EXIT("玩家退出"){

		@Override
		public void call(ClientSide clientSide, String data) {

			Room room = ServerContains.getRoom(clientSide.getRoomId());

			if(room != null) {
				String result = MapHelper.newInstance()
						.put("roomId", room.getId())
						.put("exitClientId", clientSide.getId())
						.put("exitClientNickname", clientSide.getNickname())
						.json();
				for(ClientSide client: room.getClientSideList()) {
					if(client.getRole() == ClientRole.PLAYER){
						ChannelUtils.pushToClient(client.getChannel(), ClientEventCode.CODE_CLIENT_EXIT, result);
						client.init();
					}
				}
				ServerContains.removeRoom(room.getId());
			}
		}
	},

	CODE_CLIENT_OFFLINE("玩家离线"){

		@Override
		public void call(ClientSide clientSide, String data) {

			Room room = ServerContains.getRoom(clientSide.getRoomId());

			if(room != null) {
				String result = MapHelper.newInstance()
						.put("roomId", room.getId())
						.put("exitClientId", clientSide.getId())
						.put("exitClientNickname", clientSide.getNickname())
						.json();
				for(ClientSide client: room.getClientSideList()) {
					if(client.getRole() == ClientRole.PLAYER){
						if(client.getId() != clientSide.getId()){
							ChannelUtils.pushToClient(client.getChannel(), ClientEventCode.CODE_CLIENT_EXIT, result);
							client.init();
						}
					}
				}
				ServerContains.removeRoom(room.getId());
			}

			ServerContains.CLIENT_SIDE_MAP.remove(clientSide.getId());
		}
	},

	CODE_CLIENT_NICKNAME_SET("设置昵称"){
		public static final int NICKNAME_MAX_LENGTH = 10;

		@Override
		public void call(ClientSide client, String nickname) {

			if (nickname.trim().length() > NICKNAME_MAX_LENGTH) {
				String result = MapHelper.newInstance().put("invalidLength", nickname.trim().length()).json();
				ChannelUtils.pushToClient(client.getChannel(), ClientEventCode.CODE_CLIENT_NICKNAME_SET, result);
			}else{
				ServerContains.CLIENT_SIDE_MAP.get(client.getId()).setNickname(nickname);
				ChannelUtils.pushToClient(client.getChannel(), ClientEventCode.CODE_SHOW_OPTIONS, null);
			}
		}
	},

	CODE_CLIENT_HEAD_BEAT("不出"){
		@Override
		public void call(ClientSide client, String data) {

		}
	},

	CODE_ROOM_CREATE("创建PVP房间"){
		@Override
		public void call(ClientSide clientSide, String data) {

			Room room = new Room(ServerContains.getServerId());
			room.setStatus(RoomStatus.BLANK);
			room.setType(RoomType.PVP);
			room.setRoomOwner(clientSide.getNickname());
			room.getClientSideMap().put(clientSide.getId(), clientSide);
			room.getClientSideList().add(clientSide);
			room.setCurrentSellClient(clientSide.getId());
			room.setCreateTime(System.currentTimeMillis());
			room.setLastFlushTime(System.currentTimeMillis());

			clientSide.setRoomId(room.getId());
			ServerContains.addRoom(room);

			ChannelUtils.pushToClient(clientSide.getChannel(), ClientEventCode.CODE_ROOM_CREATE_SUCCESS, Noson.reversal(room));
		}
	},

	CODE_ROOM_CREATE_PVE("创建PVE房间"){
		@Override
		public void call(ClientSide clientSide, String data) {

			int difficultyCoefficient = Integer.valueOf(data);
			if(RobotDecisionMakers.contains(difficultyCoefficient)) {

				Room room = new Room(ServerContains.getServerId());
				room.setType(RoomType.PVE);
				room.setStatus(RoomStatus.BLANK);
				room.setRoomOwner(clientSide.getNickname());
				room.getClientSideMap().put(clientSide.getId(), clientSide);
				room.getClientSideList().add(clientSide);
				room.setCurrentSellClient(clientSide.getId());
				room.setCreateTime(System.currentTimeMillis());
				room.setDifficultyCoefficient(difficultyCoefficient);

				clientSide.setRoomId(room.getId());
				ServerContains.addRoom(room);

				ClientSide preClient = clientSide;
				//Add robots
				for(int index = 1; index < 3; index ++) {
					ClientSide robot = new ClientSide(- ServerContains.getClientId(), ClientStatus.PLAYING, null);
					robot.setNickname("robot_" + index);
					robot.setRole(ClientRole.ROBOT);
					preClient.setNext(robot);
					robot.setPre(preClient);
					robot.setRoomId(room.getId());
					room.getClientSideMap().put(robot.getId(), robot);
					room.getClientSideList().add(robot);

					preClient = robot;
					ServerContains.CLIENT_SIDE_MAP.put(robot.getId(), robot);
				}
				preClient.setNext(clientSide);
				clientSide.setPre(preClient);

				ServerEventCode.CODE_GAME_STARTING.call(clientSide, String.valueOf(room.getId()));
			}else {
				ChannelUtils.pushToClient(clientSide.getChannel(), ClientEventCode.CODE_PVE_DIFFICULTY_NOT_SUPPORT, null);
			}

		}
	},

	CODE_GET_ROOMS("获取房间列表"){
		@Override
		public void call(ClientSide clientSide, String data) {
			List<Map<String, Object>> roomList = new ArrayList<>(ServerContains.getRoomMap().size());
			for(Map.Entry<Integer, Room> entry: ServerContains.getRoomMap().entrySet()) {
				Room room = entry.getValue();
				roomList.add(MapHelper.newInstance()
						.put("roomId", room.getId())
						.put("roomOwner", room.getRoomOwner())
						.put("roomClientCount", room.getClientSideList().size())
						.put("roomType", room.getType())
						.map());
			}
			ChannelUtils.pushToClient(clientSide.getChannel(), ClientEventCode.CODE_SHOW_ROOMS, Noson.reversal(roomList));
		}
	},

	CODE_ROOM_JOIN("加入房间"){
		@Override
		public void call(ClientSide clientSide, String data) {

			Room room = ServerContains.getRoom(Integer.valueOf(data));

			if(room == null) {
				String result = MapHelper.newInstance()
						.put("roomId", data)
						.json();
				ChannelUtils.pushToClient(clientSide.getChannel(), ClientEventCode.CODE_ROOM_JOIN_FAIL_BY_INEXIST, result);
			}else {
				if(room.getClientSideList().size() == 3) {
					String result = MapHelper.newInstance()
							.put("roomId", room.getId())
							.put("roomOwner", room.getRoomOwner())
							.json();
					ChannelUtils.pushToClient(clientSide.getChannel(), ClientEventCode.CODE_ROOM_JOIN_FAIL_BY_FULL, result);
				}else {
					clientSide.setRoomId(room.getId());

					ConcurrentSkipListMap<Integer, ClientSide> roomClientMap = (ConcurrentSkipListMap<Integer, ClientSide>) room.getClientSideMap();
					LinkedList<ClientSide> roomClientList = room.getClientSideList();

					if(roomClientList.size() > 0){
						ClientSide pre = roomClientList.getLast();
						pre.setNext(clientSide);
						clientSide.setPre(pre);
					}

					roomClientList.add(clientSide);
					roomClientMap.put(clientSide.getId(), clientSide);

					if(roomClientMap.size() == 3) {
						clientSide.setNext(roomClientList.getFirst());
						roomClientList.getFirst().setPre(clientSide);

						ServerEventCode.CODE_GAME_STARTING.call(clientSide, String.valueOf(room.getId()));
					}else {
						room.setStatus(RoomStatus.WAIT);

						String result = MapHelper.newInstance()
								.put("clientId", clientSide.getId())
								.put("clientNickname", clientSide.getNickname())
								.put("roomId", room.getId())
								.put("roomOwner", room.getRoomOwner())
								.put("roomClientCount", room.getClientSideList().size())
								.json();
						for(ClientSide client: roomClientMap.values()) {
							ChannelUtils.pushToClient(client.getChannel(), ClientEventCode.CODE_ROOM_JOIN_SUCCESS, result);
						}
					}
				}


			}
		}

	},

	CODE_GAME_STARTING("游戏开始"){
		@Override
		public void call(ClientSide clientSide, String data) {

			Room room = ServerContains.getRoom(clientSide.getRoomId());

			LinkedList<ClientSide> roomClientList = room.getClientSideList();

			// Send the points of poker
			List<List<Poker>> pokersList = PokerHelper.distributePoker();
			int cursor = 0;
			for(ClientSide client: roomClientList){
				client.setPokers(pokersList.get(cursor ++));
			}
			room.setLandlordPokers(pokersList.get(3));

			// Push information about the robber
			int startGrabIndex = (int)(Math.random() * 3);
			ClientSide startGrabClient = roomClientList.get(startGrabIndex);
			room.setCurrentSellClient(startGrabClient.getId());

			// Push start game messages
			room.setStatus(RoomStatus.STARTING);


			for(ClientSide client: roomClientList) {
				client.setType(ClientType.PEASANT);

				String result = MapHelper.newInstance()
						.put("roomId", room.getId())
						.put("roomOwner", room.getRoomOwner())
						.put("roomClientCount", room.getClientSideList().size())
						.put("nextClientNickname", startGrabClient.getNickname())
						.put("nextClientId", startGrabClient.getId())
						.put("pokers", client.getPokers())
						.json();

				if(client.getRole() == ClientRole.PLAYER) {
					ChannelUtils.pushToClient(client.getChannel(), ClientEventCode.CODE_GAME_STARTING, result);
				}else {
					if(startGrabClient.getId() == client.getId()) {
						RobotEventCode.CODE_GAME_LANDLORD_ELECT.call(client, result);
					}
				}

			}


		}
	},

	CODE_GAME_LANDLORD_ELECT("抢地主"){
		@Override
		public void call(ClientSide clientSide, String data) {

			Room room = ServerContains.getRoom(clientSide.getRoomId());

			if(room != null) {
				boolean isY = Boolean.valueOf(data);
				if(isY){
					clientSide.getPokers().addAll(room.getLandlordPokers());
					PokerHelper.sortPoker(clientSide.getPokers());

					int currentClientId = clientSide.getId();
					room.setLandlordId(currentClientId);
					room.setLastSellClient(currentClientId);
					room.setCurrentSellClient(currentClientId);
					clientSide.setType(ClientType.LANDLORD);

					for(ClientSide client: room.getClientSideList()){
						String result = MapHelper.newInstance()
								.put("roomId", room.getId())
								.put("roomOwner", room.getRoomOwner())
								.put("roomClientCount", room.getClientSideList().size())
								.put("landlordNickname", clientSide.getNickname())
								.put("landlordId", clientSide.getId())
								.put("additionalPokers", room.getLandlordPokers())
								.json();

						if(client.getRole() == ClientRole.PLAYER) {
							ChannelUtils.pushToClient(client.getChannel(), ClientEventCode.CODE_GAME_LANDLORD_CONFIRM, result);
						}else {
							if(currentClientId == client.getId()) {
								RobotEventCode.CODE_GAME_POKER_PLAY.call(client, result);
							}
						}
					}
				}else{
					if(clientSide.getNext().getId() == room.getLandlordId()){
						for(ClientSide client: room.getClientSideList()){
							if(client.getRole() == ClientRole.PLAYER) {
								ChannelUtils.pushToClient(client.getChannel(), ClientEventCode.CODE_GAME_LANDLORD_CYCLE, null);
							}
						}
						ServerEventCode.CODE_GAME_STARTING.call(clientSide, null);
					}else{
						ClientSide turnClientSide = clientSide.getNext();
						room.setCurrentSellClient(turnClientSide.getId());
						String result = MapHelper.newInstance()
								.put("roomId", room.getId())
								.put("roomOwner", room.getRoomOwner())
								.put("roomClientCount", room.getClientSideList().size())
								.put("preClientNickname", clientSide.getNickname())
								.put("nextClientNickname", turnClientSide.getNickname())
								.put("nextClientId", turnClientSide.getId())
								.json();

						for(ClientSide client: room.getClientSideList()) {
							if(client.getRole() == ClientRole.PLAYER) {
								ChannelUtils.pushToClient(client.getChannel(), ClientEventCode.CODE_GAME_LANDLORD_ELECT, result);
							}else {
								if(client.getId() == turnClientSide.getId()) {
									RobotEventCode.CODE_GAME_LANDLORD_ELECT.call(client, result);
								}
							}
						}
					}
				}
			}else {
//			ChannelUtils.pushToClient(clientSide.getChannel(), ClientEventCode.CODE_ROOM_PLAY_FAIL_BY_INEXIST, null);
			}
		}
	},

	CODE_GAME_POKER_PLAY("出牌环节"){
		@Override
		public void call(ClientSide clientSide, String data) {
			Room room = ServerContains.getRoom(clientSide.getRoomId());
			if(room != null) {
				if(room.getCurrentSellClient() == clientSide.getId()) {
					Character[] options = Noson.convert(data, Character[].class);
					int[] indexes = PokerHelper.getIndexes(options, clientSide.getPokers());
					if(PokerHelper.checkPokerIndex(indexes, clientSide.getPokers())){
						boolean sellFlag = true;

						List<Poker> currentPokers = PokerHelper.getPoker(indexes, clientSide.getPokers());
						PokerSell currentPokerShell = PokerHelper.checkPokerType(currentPokers);
						if(currentPokerShell.getSellType() != SellType.ILLEGAL) {
							if(room.getLastSellClient() != clientSide.getId() && room.getLastPokerShell() != null){
								PokerSell lastPokerShell = room.getLastPokerShell();

								if((lastPokerShell.getSellType() != currentPokerShell.getSellType() || lastPokerShell.getSellPokers().size() != currentPokerShell.getSellPokers().size()) && currentPokerShell.getSellType() != SellType.BOMB && currentPokerShell.getSellType() != SellType.KING_BOMB) {
									String result = MapHelper.newInstance()
											.put("playType", currentPokerShell.getSellType())
											.put("playCount", currentPokerShell.getSellPokers().size())
											.put("preType", lastPokerShell.getSellType())
											.put("preCount", lastPokerShell.getSellPokers().size())
											.json();
									sellFlag = false;
									ChannelUtils.pushToClient(clientSide.getChannel(), ClientEventCode.CODE_GAME_POKER_PLAY_MISMATCH, result);
								}else if(lastPokerShell.getScore() >= currentPokerShell.getScore()) {
									String result = MapHelper.newInstance()
											.put("playScore", currentPokerShell.getScore())
											.put("preScore", lastPokerShell.getScore())
											.json();
									sellFlag = false;
									ChannelUtils.pushToClient(clientSide.getChannel(), ClientEventCode.CODE_GAME_POKER_PLAY_LESS, result);
								}
							}
						}else {
							sellFlag = false;
							ChannelUtils.pushToClient(clientSide.getChannel(), ClientEventCode.CODE_GAME_POKER_PLAY_INVALID, null);
						}

						if(sellFlag) {
							ClientSide next = clientSide.getNext();

							room.setLastSellClient(clientSide.getId());
							room.setLastPokerShell(currentPokerShell);
							room.setCurrentSellClient(next.getId());

							clientSide.getPokers().removeAll(currentPokers);
							MapHelper mapHelper = MapHelper.newInstance()
									.put("clientId", clientSide.getId())
									.put("clientNickname", clientSide.getNickname())
									.put("clientType", clientSide.getType())
									.put("pokers", currentPokers);

							if(! clientSide.getPokers().isEmpty()) {
								mapHelper.put("sellClinetNickname", next.getNickname());
							}

							String result = mapHelper.json();

							for(ClientSide client: room.getClientSideList()) {
								if(client.getRole() == ClientRole.PLAYER) {
									ChannelUtils.pushToClient(client.getChannel(), ClientEventCode.CODE_SHOW_POKERS, result);
								}
							}

							if(clientSide.getPokers().isEmpty()) {
								result = MapHelper.newInstance()
										.put("winnerNickname", clientSide.getNickname())
										.put("winnerType", clientSide.getType())
										.json();

								for(ClientSide client: room.getClientSideList()) {
									if(client.getRole() == ClientRole.PLAYER) {
										ChannelUtils.pushToClient(client.getChannel(), ClientEventCode.CODE_GAME_OVER, result);
									}
								}
								ServerEventCode.CODE_CLIENT_EXIT.call(clientSide, data);
							}else {
								if(next.getRole() == ClientRole.PLAYER) {
									ServerEventCode.CODE_GAME_POKER_PLAY_REDIRECT.call(next, result);
								}else {
									RobotEventCode.CODE_GAME_POKER_PLAY.call(next, data);
								}
							}
						}
					}else{
						ChannelUtils.pushToClient(clientSide.getChannel(), ClientEventCode.CODE_GAME_POKER_PLAY_INVALID, null);
					}
				}else {
					ChannelUtils.pushToClient(clientSide.getChannel(), ClientEventCode.CODE_GAME_POKER_PLAY_ORDER_ERROR, null);
				}
			}else {
//			ChannelUtils.pushToClient(clientSide.getChannel(), ClientEventCode.CODE_ROOM_PLAY_FAIL_BY_INEXIST, null);
			}
		}
	},

	CODE_GAME_POKER_PLAY_REDIRECT("出牌重定向"){
		@Override
		public void call(ClientSide clientSide, String data) {
			Room room = ServerContains.getRoom(clientSide.getRoomId());

			List<Map<String, Object>> clientInfos = new ArrayList<Map<String,Object>>(3);
			for(ClientSide client: room.getClientSideList()){
				if(clientSide.getId() != client.getId()){
					clientInfos.add(MapHelper.newInstance()
							.put("clientId", client.getId())
							.put("clientNickname", client.getNickname())
							.put("type", client.getType())
							.put("surplus", client.getPokers().size())
							.put("position", clientSide.getPre().getId() == client.getId() ? "UP" : "DOWN")
							.map());
				}
			}

			String result = MapHelper.newInstance()
					.put("pokers", clientSide.getPokers())
					.put("clientInfos", clientInfos)
					.put("sellClientId", room.getCurrentSellClient())
					.put("sellClinetNickname", ServerContains.CLIENT_SIDE_MAP.get(room.getCurrentSellClient()).getNickname())
					.json();

			ChannelUtils.pushToClient(clientSide.getChannel(), ClientEventCode.CODE_GAME_POKER_PLAY_REDIRECT, result);
		}
	},

	CODE_GAME_POKER_PLAY_PASS("不出"){
		@Override
		public void call(ClientSide clientSide, String data) {
			Room room = ServerContains.getRoom(clientSide.getRoomId());

			if(room != null) {
				if(room.getCurrentSellClient() == clientSide.getId()) {
					if(clientSide.getId() != room.getLastSellClient()) {
						ClientSide turnClient = clientSide.getNext();

						room.setCurrentSellClient(turnClient.getId());

						for(ClientSide client: room.getClientSideList()) {
							String result = MapHelper.newInstance()
									.put("clientId", clientSide.getId())
									.put("clientNickname", clientSide.getNickname())
									.put("nextClientId", turnClient.getId())
									.put("nextClientNickname", turnClient.getNickname())
									.json();
							if(client.getRole() == ClientRole.PLAYER) {
								ChannelUtils.pushToClient(client.getChannel(), ClientEventCode.CODE_GAME_POKER_PLAY_PASS, result);
							}else {
								if(client.getId() == turnClient.getId()) {
									RobotEventCode.CODE_GAME_POKER_PLAY.call(turnClient, data);
								}
							}
						}
					}else {
						ChannelUtils.pushToClient(clientSide.getChannel(), ClientEventCode.CODE_GAME_POKER_PLAY_CANT_PASS, null);
					}
				}else {
					ChannelUtils.pushToClient(clientSide.getChannel(), ClientEventCode.CODE_GAME_POKER_PLAY_ORDER_ERROR, null);
				}
			}else {
//			ChannelUtils.pushToClient(clientSide.getChannel(), ClientEventCode.CODE_ROOM_PLAY_FAIL_BY_INEXIST, null);
			}
		}
	}

	;
	
	private String msg;

	private ServerEventCode(String msg) {
		this.msg = msg;
	}

	public final String getMsg() {
		return msg;
	}

	public final void setMsg(String msg) {
		this.msg = msg;
	}

	public static class ServerContains {

		/**
		 * Server port
		 */
		public static int port = 1024;

		/**
		 * The map of server side
		 */
		private final static Map<Integer, Room> ROOM_MAP = new ConcurrentSkipListMap<>();

		/**
		 * The list of client side
		 */
		public final static Map<Integer, ClientSide> CLIENT_SIDE_MAP = new ConcurrentSkipListMap<>();

		private final static AtomicInteger CLIENT_ATOMIC_ID = new AtomicInteger(1);

		public final static Map<String, Integer> CHANNEL_ID_MAP = new ConcurrentHashMap<>();

		private final static AtomicInteger SERVER_ATOMIC_ID = new AtomicInteger(1);

		public final static int getClientId() {
			return CLIENT_ATOMIC_ID.getAndIncrement();
		}

		public final static int getServerId() {
			return SERVER_ATOMIC_ID.getAndIncrement();
		}

		public final static ThreadPoolExecutor THREAD_EXCUTER = new ThreadPoolExecutor(500, 500, 0, TimeUnit.MILLISECONDS,
				new LinkedBlockingQueue<Runnable>());

		/**
		 * Get room by id, with flush time
		 *
		 * @param id room id
		 * @return
		 */
		public final static Room getRoom(int id){
			Room room = ROOM_MAP.get(id);
			if(room != null){
				room.setLastFlushTime(System.currentTimeMillis());
			}
			return room;
		}

		public final static Room getRoomNotFlushTime(int id){
			Room room = ROOM_MAP.get(id);

			return room;
		}

		public final static Map<Integer, Room> getRoomMap(){
			return ROOM_MAP;
		}

		public final static Room removeRoom(int id){
			return ROOM_MAP.remove(id);
		}

		public final static Room addRoom(Room room){
			return ROOM_MAP.put(room.getId(), room);
		}
	}

	public abstract void call(ClientSide client, String data);


}

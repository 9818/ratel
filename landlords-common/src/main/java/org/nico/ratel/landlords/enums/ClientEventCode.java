package org.nico.ratel.landlords.enums;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import org.nico.noson.Noson;
import org.nico.noson.entity.NoType;
import org.nico.noson.util.string.StringUtils;
import org.nico.ratel.landlords.channel.ChannelUtils;
import org.nico.ratel.landlords.entity.Poker;
import org.nico.ratel.landlords.entity.Room;
import org.nico.ratel.landlords.helper.MapHelper;
import org.nico.ratel.landlords.helper.PokerHelper;
import org.nico.ratel.landlords.print.FormatPrinter;
import org.nico.ratel.landlords.print.SimplePrinter;
import org.nico.ratel.landlords.print.SimpleWriter;
import org.nico.ratel.landlords.utils.OptionsUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public enum ClientEventCode implements Serializable{



	CODE_CLIENT_NICKNAME_SET("设置昵称"){

		@Override
		public void call(Channel channel, String data) {

			// If it is not the first time that the user is prompted to enter nickname
			// If first time, data = null or "" otherwise not empty
			if (StringUtils.isNotBlank(data)) {
				Map<String, Object> dataMap = MapHelper.parser(data);
				if (dataMap.containsKey("invalidLength")) {
					SimplePrinter.printNotice("Your nickname length was invalid: " + dataMap.get("invalidLength"));
				}
			}
			SimplePrinter.printNotice("Please set your nickname (upto " + NICKNAME_MAX_LENGTH + " characters)");
			String nickname = SimpleWriter.write("nickname");

			// If the length of nickname is more that NICKNAME_MAX_LENGTH
			if (nickname.trim().length() > NICKNAME_MAX_LENGTH) {
				String result = MapHelper.newInstance().put("invalidLength", nickname.trim().length()).json();
				ClientEventCode.CODE_CLIENT_NICKNAME_SET.call(channel, result);
			}else{
				pushToServer(channel, ServerEventCode.CODE_CLIENT_NICKNAME_SET, nickname);
			}
		}
	},
	
	CODE_CLIENT_EXIT("客户端退出"){
		@Override
		public void call(Channel channel, String data) {
			Map<String, Object> map = MapHelper.parser(data);

			Integer exitClientId = (Integer) map.get("exitClientId");

			String role = null;
			if(exitClientId == simpleClientId) {
				role = "You";
			}else {
				role = String.valueOf(map.get("exitClientNickname"));
			}
			SimplePrinter.printNotice(role + " exit from the room. Room disbanded!!\n");

			ClientEventCode.CODE_SHOW_OPTIONS.call(channel, data);
		}
	},
	
	CODE_CLIENT_KICK("客户端被踢出"){
		@Override
		public void call(Channel channel, String data) {

			SimplePrinter.printNotice("As a result of long time do not operate, be forced by the system to kick out of the room\n");

			ClientEventCode.CODE_SHOW_OPTIONS.call(channel, data);
		}
	},
	
	CODE_CLIENT_CONNECT("客户端加入成功"){
		@Override
		public void call(Channel channel, String data) {
			SimplePrinter.printNotice("Connection to server is successful. Welcome to ratel!!");
			simpleClientId = Integer.parseInt(data);
		}
	},
	
	CODE_SHOW_OPTIONS("全局选项列表"){
		@Override
		public void call(Channel channel, String data) {
			SimplePrinter.printNotice("Options: ");
			SimplePrinter.printNotice("1. PvP");
			SimplePrinter.printNotice("2. PvE");
			SimplePrinter.printNotice("3. Setting");
			SimplePrinter.printNotice("Please enter the number of options (enter [EXIT] log out)");
			String line = SimpleWriter.write("options");

			if(line.equalsIgnoreCase("EXIT")) {
				System.exit(0);
			}else {
				int choose = OptionsUtils.getOptions(line);

				if(choose == 1) {
					ClientEventCode.CODE_SHOW_OPTIONS_PVP.call(channel, data);
				}else if(choose == 2){
					ClientEventCode.CODE_SHOW_OPTIONS_PVE.call(channel, data);
				}else if(choose == 3){
					ClientEventCode.CODE_SHOW_OPTIONS_SETTING.call(channel, data);
				}else {
					SimplePrinter.printNotice("Invalid option, please choose again：");
					call(channel, data);
				}
			}
		}

	},
	
	CODE_SHOW_OPTIONS_SETTING("设置选项"){
		@Override
		public void call(Channel channel, String data) {
			SimplePrinter.printNotice("Setting: ");
			SimplePrinter.printNotice("1. Card with shape edges (Default)");
			SimplePrinter.printNotice("2. Card with rounded edges");
			SimplePrinter.printNotice("3. Text Only with types");
			SimplePrinter.printNotice("4. Text Only without types");
			SimplePrinter.printNotice("5. Unicode Cards");

			SimplePrinter.printNotice("Please enter the number of setting (enter [BACK] return options list)");
			String line = SimpleWriter.write("setting");

			if(line.equalsIgnoreCase("BACK")) {
				ClientEventCode.CODE_SHOW_OPTIONS.call(channel, data);
			}else {
				int choose = OptionsUtils.getOptions(line);

				if(choose >=1 && choose <= 5){
					PokerHelper.pokerPrinterType = choose - 1;
					ClientEventCode.CODE_SHOW_OPTIONS.call(channel, data);
				} else {
					SimplePrinter.printNotice("Invalid setting, please choose again：");
					call(channel, data);
				}
			}
		}
	},
	
	CODE_SHOW_OPTIONS_PVP("玩家对战选项"){
		@Override
		public void call(Channel channel, String data) {
			SimplePrinter.printNotice("PVP: ");
			SimplePrinter.printNotice("1. Create Room");
			SimplePrinter.printNotice("2. Room List");
			SimplePrinter.printNotice("3. Join Room");
			SimplePrinter.printNotice("Please enter the number of options (enter [BACK] return options list)");
			String line = SimpleWriter.write("pvp");

			if(line.equalsIgnoreCase("BACK")) {
				ClientEventCode.CODE_SHOW_OPTIONS.call(channel, data);
			}else {
				int choose = OptionsUtils.getOptions(line);

				if(choose == 1) {
					pushToServer(channel, ServerEventCode.CODE_ROOM_CREATE, null);
				}else if(choose == 2){
					pushToServer(channel, ServerEventCode.CODE_GET_ROOMS, null);
				}else if(choose == 3){
					SimplePrinter.printNotice("Please enter the room id you want to join (enter [BACK] return options list)");
					line = SimpleWriter.write("roomid");

					if(line.equalsIgnoreCase("BACK")) {
						call(channel, data);
					}else {
						int option = OptionsUtils.getOptions(line);
						if(line == null || option < 1) {
							SimplePrinter.printNotice("Invalid options, please choose again：");
							call(channel, data);
						}else{
							pushToServer(channel, ServerEventCode.CODE_ROOM_JOIN, String.valueOf(option));
						}
					}
				}else {
					SimplePrinter.printNotice("Invalid option, please choose again：");
					call(channel, data);
				}
			}

		}
	},
	
	CODE_SHOW_OPTIONS_PVE("人机对战选项"){
		@Override
		public void call(Channel channel, String data) {
			SimplePrinter.printNotice("PVE: ");
			SimplePrinter.printNotice("1. Simple Model");
			SimplePrinter.printNotice("2. Medium Model");
			SimplePrinter.printNotice("3. Difficulty Model");
			SimplePrinter.printNotice("Please enter the number of options (enter [BACK] return options list)");
			String line = SimpleWriter.write("pve");

			if(line.equalsIgnoreCase("BACK")) {
				ClientEventCode.CODE_SHOW_OPTIONS.call(channel, data);
			}else {
				int choose = OptionsUtils.getOptions(line);

				if(0 < choose && choose < 4) {
					initLastSellInfo();
					pushToServer(channel, ServerEventCode.CODE_ROOM_CREATE_PVE, String.valueOf(choose));
				}else {
					SimplePrinter.printNotice("Invalid option, please choose again：");
					call(channel, data);
				}
			}

		}
	},
	
	CODE_SHOW_ROOMS("展示房间列表"){
		@Override
		public void call(Channel channel, String data) {

			List<Map<String, Object>> roomList = Noson.convert(data, new NoType<List<Map<String, Object>>>() {});
			if(roomList != null && ! roomList.isEmpty()){
				// "COUNT" begins after NICKNAME_MAX_LENGTH characters. The dash means that the string is left-justified.
				String format = "#\t%s\t|\t%-" + NICKNAME_MAX_LENGTH + "s\t|\t%s\t|\t%s\t#\n";
				FormatPrinter.printNotice(format, "ID", "OWNER", "COUNT", "TYPE");
				for(Map<String, Object> room: roomList) {
					FormatPrinter.printNotice(format, room.get("roomId"), room.get("roomOwner"), room.get("roomClientCount"), room.get("roomType"));
				}
				SimplePrinter.printNotice("");
				ClientEventCode.CODE_SHOW_OPTIONS_PVP.call(channel, data);
			}else {
				SimplePrinter.printNotice("No available room, please create a room ！");
				ClientEventCode.CODE_SHOW_OPTIONS_PVP.call(channel, data);
			}
		}
	},
	
	CODE_SHOW_POKERS("展示Poker"){
		@Override
		public void call(Channel channel, String data) {

			Map<String, Object> map = MapHelper.parser(data);

			lastSellClientNickname = (String) map.get("clientNickname");
			lastSellClientType = (String) map.get("clientType");

			SimplePrinter.printNotice(lastSellClientNickname + "[" + lastSellClientType + "] played:");
			lastPokers = Noson.convert(map.get("pokers"), new NoType<List<Poker>>() {});
			SimplePrinter.printPokers(lastPokers);

			if(map.containsKey("sellClinetNickname")) {
				SimplePrinter.printNotice("Next player is " + map.get("sellClinetNickname") + ". Please wait for him to play his pokers.");
			}
		}
	},
	
	CODE_ROOM_CREATE_SUCCESS("创建房间成功"){
		@Override
		public void call(Channel channel, String data) {

			Room room = Noson.convert(data, Room.class);

			initLastSellInfo();

			SimplePrinter.printNotice("You have created a room with id " + room.getId());
			SimplePrinter.printNotice("Please wait for other players to join !");

		}
	},
	
	CODE_ROOM_JOIN_SUCCESS("加入房间成功"){
		@Override
		public void call(Channel channel, String data) {
			Map<String, Object> map = MapHelper.parser(data);

			initLastSellInfo();

			int joinClientId = (int) map.get("clientId");
			if(simpleClientId == joinClientId) {
				SimplePrinter.printNotice("You have joined room：" + map.get("roomId") + ". There are " + map.get("roomClientCount") + " players in the room now.");
				SimplePrinter.printNotice("Please wait for other players to join, start a good game when the room player reaches three !");
			}else {
				SimplePrinter.printNotice(map.get("clientNickname") + " joined room, the current number of room player is " + map.get("roomClientCount"));
			}

		}
	},
	
	CODE_ROOM_JOIN_FAIL_BY_FULL("房间人数已满"){
		@Override
		public void call(Channel channel, String data) {
			Map<String, Object> dataMap = MapHelper.parser(data);

			SimplePrinter.printNotice("Join room failed. Room " + dataMap.get("roomId") + " player count is full!");
			ClientEventCode.CODE_SHOW_OPTIONS.call(channel, data);
		}

	},
	
	CODE_ROOM_JOIN_FAIL_BY_INEXIST("加入-房间不存在"){
		@Override
		public void call(Channel channel, String data) {

			SimplePrinter.printNotice("Play failed. Room already dissolution!");
			ClientEventCode.CODE_SHOW_OPTIONS.call(channel, data);
		}

	},
	
	CODE_ROOM_PLAY_FAIL_BY_INEXIST1("出牌-房间不存在"){
		@Override
		public void call(Channel channel, String data) {

		}
	},
	
	CODE_GAME_STARTING("开始游戏"){
		@Override
		public void call(Channel channel, String data) {

			Map<String, Object> map = MapHelper.parser(data);

			SimplePrinter.printNotice("Game starting !!");

			List<Poker> pokers = Noson.convert(map.get("pokers"), new NoType<List<Poker>>() {});

			SimplePrinter.printNotice("");
			SimplePrinter.printNotice("Your pokers are");
			SimplePrinter.printPokers(pokers);

			ClientEventCode.CODE_GAME_LANDLORD_ELECT.call(channel, data);
		}
	},
	
	CODE_GAME_LANDLORD_ELECT("抢地主"){
		@Override
		public void call(Channel channel, String data) {
			Map<String, Object> map = MapHelper.parser(data);
			int turnClientId = (int) map.get("nextClientId");

			if(map.containsKey("preClientNickname")) {
				SimplePrinter.printNotice(map.get("preClientNickname") + " don't rob the landlord!");
			}

			if(turnClientId == simpleClientId) {
				SimplePrinter.printNotice("It's your turn. Do you want to rob the landlord? [Y/N] (enter [EXIT] to exit current room)");
				String line = SimpleWriter.write("Y/N");
				if(line.equalsIgnoreCase("EXIT")) {
					pushToServer(channel, ServerEventCode.CODE_CLIENT_EXIT);
				}else if(line.equalsIgnoreCase("Y")){
					pushToServer(channel, ServerEventCode.CODE_GAME_LANDLORD_ELECT, "TRUE");
				}else if(line.equalsIgnoreCase("N")){
					pushToServer(channel, ServerEventCode.CODE_GAME_LANDLORD_ELECT, "FALSE");
				}else{
					SimplePrinter.printNotice("Invalid options");
					call(channel, data);
				}
			}else {
				SimplePrinter.printNotice("It's " + map.get("nextClientNickname") + "'s turn. Please wait patiently for his/her confirmation !");
			}

		}
	},
	
	CODE_GAME_LANDLORD_CONFIRM("地主确认"){

		@Override
		public void call(Channel channel, String data) {
			Map<String, Object> map = MapHelper.parser(data);

			String landlordNickname = String.valueOf(map.get("landlordNickname"));

			SimplePrinter.printNotice(landlordNickname + " grabbed the landlord and got extra three poker shots");

			List<Poker> additionalPokers = Noson.convert(map.get("additionalPokers"), new NoType<List<Poker>>() {});
			SimplePrinter.printPokers(additionalPokers);

			pushToServer(channel, ServerEventCode.CODE_GAME_POKER_PLAY_REDIRECT);
		}
	},
	
	CODE_GAME_LANDLORD_CYCLE("地主一轮确认结束"){
		@Override
		public void call(Channel channel, String data) {
			SimplePrinter.printNotice("No player takes the landlord, so redealing cards.");

		}
	},
	
	CODE_GAME_POKER_PLAY("出牌回合"){
		@Override
		public void call(Channel channel, String data) {
			Map<String, Object> map = MapHelper.parser(data);

			SimplePrinter.printNotice("It's your turn to play, your pokers are as follows: ");
			List<Poker> pokers = Noson.convert(map.get("pokers"), new NoType<List<Poker>>() {});
			SimplePrinter.printPokers(pokers);


			SimplePrinter.printNotice("Please enter the card you came up with (enter [EXIT] to exit current room, enter [PASS] to jump current round)");
			String line = SimpleWriter.write("card");

			if(line == null){
				SimplePrinter.printNotice("Invalid enter");
				call(channel, data);
			}else{
				if(line.equalsIgnoreCase("PASS")) {
					pushToServer(channel, ServerEventCode.CODE_GAME_POKER_PLAY_PASS);
				}else if(line.equalsIgnoreCase("EXIT")){
					pushToServer(channel, ServerEventCode.CODE_CLIENT_EXIT);
				}else {
					String[] strs = line.split(" ");
					List<Character> options = new ArrayList<>();
					boolean access = true;
					for(int index = 0; index < strs.length; index ++){
						String str = strs[index];
						for(char c: str.toCharArray()) {
							if(c == ' ' || c == '\t') {
							}else {
								if(! PokerLevel.aliasContains(c)) {
									access = false;
									break;
								}else {
									options.add(c);
								}
							}
						}
					}
					if(access){
						pushToServer(channel, ServerEventCode.CODE_GAME_POKER_PLAY, Noson.reversal(options.toArray(new Character[] {})));
					}else{
						SimplePrinter.printNotice("Invalid enter");

						if(lastPokers != null) {
							SimplePrinter.printNotice(lastSellClientNickname + "[" + lastSellClientType + "] played:");
							SimplePrinter.printPokers(lastPokers);
						}

						call(channel, data);
					}
				}
			}

		}
	},
	
	CODE_GAME_POKER_PLAY_REDIRECT("出牌重定向"){
		private String[] choose = new String[]{"UP", "DOWN"};

		private String format = "\n[%-4s] %-" + NICKNAME_MAX_LENGTH + "s  surplus %-2s [%-8s]";
		@Override
		public void call(Channel channel, String data) {

			Map<String, Object> map = MapHelper.parser(data);

			int sellClientId = (int) map.get("sellClientId");

			List<Map<String, Object>> clientInfos = (List<Map<String, Object>>) map.get("clientInfos");

			for(int index = 0; index < 2; index ++){
				for(Map<String, Object> clientInfo: clientInfos) {
					String position = (String) clientInfo.get("position");
					if(position.equalsIgnoreCase(choose[index])){
						FormatPrinter.printNotice(format, clientInfo.get("position"), clientInfo.get("clientNickname"), clientInfo.get("surplus"), clientInfo.get("type"));
					}
				}
			}
			SimplePrinter.printNotice("");

			if(sellClientId == simpleClientId) {
				ClientEventCode.CODE_GAME_POKER_PLAY.call(channel, data);
			}else {
				SimplePrinter.printNotice("Next player is " + map.get("sellClinetNickname") + ". Please wait for him to play his cards.");
			}
		}
	},
	
	CODE_GAME_POKER_PLAY_MISMATCH("出牌不匹配"){
		@Override
		public void call(Channel channel, String data) {
			Map<String, Object> map = MapHelper.parser(data);

			SimplePrinter.printNotice("Your pokers' type is " + map.get("playType") + " (" + map.get("playCount") + ") but previous pokers' type is " + map.get("preType") + " (" + map.get("preCount") + "), mismatch !!");

			if(lastPokers != null) {
				SimplePrinter.printNotice(lastSellClientNickname + "[" + lastSellClientType + "] played:");
				SimplePrinter.printPokers(lastPokers);
			}

			pushToServer(channel, ServerEventCode.CODE_GAME_POKER_PLAY_REDIRECT);
		}
	},
	
	CODE_GAME_POKER_PLAY_LESS("出牌太小"){
		@Override
		public void call(Channel channel, String data) {
			SimplePrinter.printNotice("Your pokers' type has lower rank than the previous. You could not play this combination !!");

			if(lastPokers != null) {
				SimplePrinter.printNotice(lastSellClientNickname + "[" + lastSellClientType + "] played:");
				SimplePrinter.printPokers(lastPokers);
			}

			pushToServer(channel, ServerEventCode.CODE_GAME_POKER_PLAY_REDIRECT);
		}
	},
	
	CODE_GAME_POKER_PLAY_PASS("不出"){
		@Override
		public void call(Channel channel, String data) {
			Map<String, Object> map = MapHelper.parser(data);

			SimplePrinter.printNotice(map.get("clientNickname") + " passed. It is now " + map.get("nextClientNickname") + "'s turn.");

			int turnClientId = (int) map.get("nextClientId");
			if(simpleClientId == turnClientId) {
				pushToServer(channel, ServerEventCode.CODE_GAME_POKER_PLAY_REDIRECT);
			}
		}
	},
	
	CODE_GAME_POKER_PLAY_CANT_PASS("不允许不出"){
		@Override
		public void call(Channel channel, String data) {
			SimplePrinter.printNotice("You played the previous card, so you can't pass.");
			pushToServer(channel, ServerEventCode.CODE_GAME_POKER_PLAY_REDIRECT);
		}

	},
	
	CODE_GAME_POKER_PLAY_INVALID("无效"){
		@Override
		public void call(Channel channel, String data) {

			SimplePrinter.printNotice("Out pokers' format is invalid");

			if(lastPokers != null) {
				SimplePrinter.printNotice(lastSellClientNickname + "[" + lastSellClientType + "] played:");
				SimplePrinter.printPokers(lastPokers);
			}

			pushToServer(channel, ServerEventCode.CODE_GAME_POKER_PLAY_REDIRECT);
		}

	},
	
	CODE_GAME_POKER_PLAY_ORDER_ERROR("顺序错误"){

		@Override
		public void call(Channel channel, String data) {

			SimplePrinter.printNotice("Not turn you to operate, please wait other player !!");
		}

	},
	
	CODE_GAME_OVER("游戏结束"){

		@Override
		public void call(Channel channel, String data) {
			Map<String, Object> map = MapHelper.parser(data);
			SimplePrinter.printNotice("\nPlayer " + map.get("winnerNickname") + "[" + map.get("winnerType") + "]" + " won the game");
			SimplePrinter.printNotice("Game over, friendship first, competition second\n");
		}
	},
	
	CODE_PVE_DIFFICULTY_NOT_SUPPORT("人机难度不支持"){
		@Override
		public void call(Channel channel, String data) {
			SimplePrinter.printNotice("The current difficulty coefficient is not supported, please pay attention to the following.\n");
			ClientEventCode.CODE_SHOW_OPTIONS_PVE.call(channel, data);
		}

	}
	;
	
	private String msg;

	private ClientEventCode(String msg) {
		this.msg = msg;
	}

	public final String getMsg() {
		return msg;
	}

	public final void setMsg(String msg) {
		this.msg = msg;
	}

	private static int simpleClientId = -1;
	private static List<Poker> lastPokers = null;
	private static String lastSellClientNickname = null;
	private static String lastSellClientType = null;

	private static final int NICKNAME_MAX_LENGTH = 10;
	protected static void initLastSellInfo() {
		lastPokers = null;
		lastSellClientNickname = null;
		lastSellClientType = null;
	}
	protected ChannelFuture pushToServer(Channel channel, ServerEventCode code, String datas){
		return ChannelUtils.pushToServer(channel, code, datas);
	}

	protected ChannelFuture pushToServer(Channel channel, ServerEventCode code){
		return pushToServer(channel, code, null);
	}

	public abstract void call(Channel channel, String data);
	
}

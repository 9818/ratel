package org.nico.ratel.landlords.enums;

import org.nico.noson.Noson;
import org.nico.ratel.landlords.entity.ClientSide;
import org.nico.ratel.landlords.entity.Poker;
import org.nico.ratel.landlords.entity.PokerSell;
import org.nico.ratel.landlords.entity.Room;
import org.nico.ratel.landlords.helper.PokerHelper;
import org.nico.ratel.landlords.helper.TimeHelper;
import org.nico.ratel.landlords.print.SimplePrinter;
import org.nico.ratel.landlords.robot.RobotDecisionMakers;

import java.util.ArrayList;
import java.util.List;

import static org.nico.ratel.landlords.enums.ServerEventCode.ServerContains.THREAD_EXCUTER;

public enum RobotEventCode{


		CODE_GAME_LANDLORD_ELECT("抢地主"){
			@Override
			public void call(ClientSide robot, String data) {
				THREAD_EXCUTER.execute(() -> {
					Room room = ServerEventCode.ServerContains.getRoom(robot.getRoomId());

					List<Poker> landlordPokers = new ArrayList<>(20);
					landlordPokers.addAll(robot.getPokers());
					landlordPokers.addAll(room.getLandlordPokers());

					List<Poker> leftPokers = new ArrayList<>(17);
					leftPokers.addAll(robot.getPre().getPokers());

					List<Poker> rightPokers = new ArrayList<>(17);
					rightPokers.addAll(robot.getNext().getPokers());

					PokerHelper.sortPoker(landlordPokers);
					PokerHelper.sortPoker(leftPokers);
					PokerHelper.sortPoker(rightPokers);

					TimeHelper.sleep(300);
					ServerEventCode.CODE_GAME_LANDLORD_ELECT.call(robot, String.valueOf(RobotDecisionMakers.howToChooseLandlord(room.getDifficultyCoefficient(), leftPokers, rightPokers, landlordPokers)));
				});
			}
		},
		CODE_GAME_POKER_PLAY("出牌回合"){
			@Override
			public void call(ClientSide robot, String data) {
				ServerEventCode.ServerContains.THREAD_EXCUTER.execute(() -> {
					Room room = ServerEventCode.ServerContains.getRoom(robot.getRoomId());

					PokerSell lastPokerSell = null;
					PokerSell pokerSell = null;
					if(room.getLastSellClient() != robot.getId()) {
						lastPokerSell = room.getLastPokerShell();
						pokerSell = RobotDecisionMakers.howToPlayPokers(room.getDifficultyCoefficient(), lastPokerSell, robot.getPokers());
					}else {
						pokerSell = RobotDecisionMakers.howToPlayPokers(room.getDifficultyCoefficient(), null, robot.getPokers());
					}

					if(pokerSell != null && lastPokerSell != null) {
						SimplePrinter.serverLog("Robot monitoring[room:" + room.getId() + "]");
						SimplePrinter.serverLog("last  sell  -> " + lastPokerSell.toString());
						SimplePrinter.serverLog("robot sell  -> " + pokerSell.toString());
						SimplePrinter.serverLog("robot poker -> " + PokerHelper.textOnlyNoType(robot.getPokers()));
					}

					TimeHelper.sleep(300);

					if(pokerSell == null || pokerSell.getSellType() == SellType.ILLEGAL) {
						ServerEventCode.CODE_GAME_POKER_PLAY_PASS.call(robot, data);
					}else {
						Character[] cs = new Character[pokerSell.getSellPokers().size()];
						for(int index = 0; index < cs.length; index ++) {
							cs[index] = pokerSell.getSellPokers().get(index).getLevel().getAlias()[0];
						}
						ServerEventCode.CODE_GAME_POKER_PLAY.call(robot, Noson.reversal(cs));
					}
				});
			}
		};

		private String msg;

		private RobotEventCode(String msg) {
			this.msg = msg;
		}

		public final String getMsg() {
			return msg;
		}

		public final void setMsg(String msg) {
			this.msg = msg;
		}


		public abstract void call(ClientSide robot, String data);
	}